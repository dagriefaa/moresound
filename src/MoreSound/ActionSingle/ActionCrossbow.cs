﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MoreSound {
    /// <summary>
    /// Since the crossbow uses a RandomSoundController, there's not much that can be done with it.
    /// </summary>
    class ActionCrossbow : ActionSingle {

        CrossBowBlock self;

        public static ActionCrossbow Add(BlockBehaviour b) {
            float power = (b.BuildingBlock as CrossBowBlock).PowerSlider.Value;
            if (power < 7f) {
                return null;
            }
            RandomSoundController rsc = (b as CrossBowBlock).randomSoundController;
            rsc.audioclips2 = new AudioClip[] { Sound.gunSound };
            rsc.randomPitch2 = false;

            return ActionSingle.Add<ActionCrossbow>(b,
                sound: Sound.gunSound,
                volume: 0.4f,
                pitch: Mathf.Clamp(1f - ((power - 7f) * 0.04f), 0.5f, 1f),
                source: rsc.audioSource);
        }

        void Start() {
            self = GetComponent<CrossBowBlock>();
        }

        protected override void UpdateAlways() {
            if (self.ammo > 0 || (self.HasParentMachine && self.ParentMachine.InfiniteAmmoMode)) {
                this.volumeBase = 0.4f;
                this.DistanceCutoff = 0.4f * Sound.REFERENCE_DISTANCE / Sound.REFERENCE_VOLUME;
                this.DistanceCutoff *= this.DistanceCutoff;
            } 
            else {
                this.volumeBase = 0.1f;
                this.DistanceCutoff = 0.1f * Sound.REFERENCE_DISTANCE / Sound.REFERENCE_VOLUME;
                this.DistanceCutoff *= this.DistanceCutoff;
            }
        }
    }
}