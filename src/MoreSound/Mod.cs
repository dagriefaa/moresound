using Modding;
using Modding.Blocks;
using System;
using UnityEngine;

namespace MoreSound {
    public class Mod : ModEntryPoint {

        public override void OnLoad() {
            ModResource.OnAllResourcesLoaded += Sound.LoadResources;
            Events.OnBlockInit += AddSound;

            GameObject controller = GameObject.Find("ModControllerObject");
            if (!controller) {
                UnityEngine.Object.DontDestroyOnLoad(controller = new GameObject("ModControllerObject"));
            }

            if (Mods.IsModLoaded(new Guid("3c1fa3de-ec74-44e4-807c-9eced79ddd3f"))) {
                controller.AddComponent<Mapper>();
            }
            controller.AddComponent<SoundController>();

            Events.OnActiveSceneChanged += (a, b) => { Sound.ActiveVolumeTotal = 0; };
        }

        private void AddSound(Block obj) {
            if (!obj.Machine.InternalObject.isSimulating || StatMaster.isClient)
                return;

            BlockBehaviour b = obj.SimBlock.InternalObject;

            bool isWheelShaped = b.transform.localScale.x > (3 * b.transform.localScale.y)
                || b.transform.localScale.z > (3 * b.transform.localScale.y);

            bool isSmall = (BlockType)b.BlockID != BlockType.LargeWheel
                && (b.transform.localScale.x <= 0.4f
                || b.transform.localScale.y <= 0.4f
                || b.transform.localScale.z <= 0.4f);
            bool isCircular = Mathf.Abs(1 - (b.transform.localScale.x / b.transform.localScale.y)) < 0.1f;

            Fire.Add(b);

            switch ((BlockType)b.BlockID) {

                case BlockType.SmallWheel:
                    Break.Add(b, Sound.woodSnapHigh);

                    break;

                case BlockType.Cannon:
                case BlockType.ShrapnelCannon:
                    Collision.Add(b, Sound.metalImpactHeavy);
                    ActionCannon.Add(b);
                    break;

                case BlockType.WoodenPanel:
                    Collision.Add(b, Sound.woodImpactLight);
                    Break.Add(b, Sound.woodSnapHigh);
                    break;

                case BlockType.Crossbow:
                    Collision.Add(b, Sound.woodImpactLight);
                    Break.Add(b, Sound.woodSnapHigh);
                    ActionCrossbow.Add(b);
                    break;

                case BlockType.Propeller:
                case BlockType.SmallPropeller:
                    Collision.Add(b, Sound.woodImpactLight);
                    Break.Add(b, Sound.woodSnapHigh);
                    ActionPropeller.Add(b);
                    break;

                case BlockType.Unused3:
                    Collision.Add(b, Sound.woodImpactLight);
                    Break.Add(b, Sound.woodSnapHigh);
                    ActionPropeller.AddHeli(b);
                    break;

                case BlockType.Wing:
                case BlockType.WingPanel:
                case BlockType.WoodenPole:
                    Collision.Add(b, Sound.woodImpactHeavy);
                    Break.Add(b, Sound.woodSnapMid);
                    break;

                case BlockType.FlyingBlock:
                    Collision.Add(b, Sound.woodImpactHeavy);
                    Break.Add(b, Sound.woodSnapMid);
                    ActionFlyingBlock.Add(b);
                    break;

                case BlockType.Ballast:
                case BlockType.ScalingBlock:
                    Collision.Add(b, Sound.metalImpactHeavy);
                    Break.Add(b, Sound.woodSnapLow);
                    break;

                case BlockType.SingleWoodenBlock:
                case BlockType.DoubleWoodenBlock:
                case BlockType.Log:
                    Collision.Add(b, Sound.woodImpactHeavy);
                    Break.Add(b, Sound.woodSnapLow);
                    break;

                case BlockType.Grabber:
                    Collision.Add(b, Sound.metalImpactHeavy);
                    Break.Add(b, Sound.metalSnapHigh);
                    break;

                case BlockType.Decoupler:
                    Collision.Add(b, Sound.metalImpactHeavy);
                    Break.Add(b, Sound.metalSnapLow);
                    b.gameObject.AddComponent<ActionDecoupler>();
                    break;

                case BlockType.MetalBlade:
                    Collision.Add(b, Sound.metalImpactLight);
                    Break.Add(b, Sound.metalSnapMid);
                    break;

                case BlockType.Spike:
                    Break.Add(b, Sound.metalSnapMid);
                    break;

                case BlockType.Slider:
                    Collision.Add(b, Sound.woodImpactHeavy);
                    Break.Add(b, Sound.metalSnapMid);
                    break;

                case BlockType.Altimeter:
                case BlockType.Anglometer:
                case BlockType.Speedometer:
                case BlockType.LogicGate:
                case BlockType.Timer:
                    Collision.Add(b, Sound.metalImpactHeavy);
                    Break.Add(b, Sound.metalSnapMid);
                    break;

                case BlockType.Flamethrower:
                    Collision.Add(b, Sound.metalImpactHeavy);

                    Break.Add(b, Sound.metalSnapMid);

                    ActionFlamethrower.Add(b);
                    break;

                case BlockType.CircularSaw:
                case BlockType.Drill:
                    Collision.Add(b, Sound.metalImpactHeavy);
                    Break.Add(b, Sound.metalSnapMid);

                    ActionMotor.Add(b,
                        sound: Sound.motorSoundSmall,
                        pitch: Mathf.Clamp((b.BuildingBlock as CogMotorControllerHinge).SpeedSlider.Value, 0.5f, 1.2f));
                    break;

                case BlockType.BombHolder:
                case BlockType.Suspension:
                case BlockType.BallJoint:
                case BlockType.Swivel:
                case BlockType.Axle:
                case BlockType.MetalJaw:
                    Collision.Add(b, Sound.metalImpactHeavy);
                    Break.Add(b, Sound.metalSnapLow);
                    break;

                case BlockType.Piston:
                    Collision.Add(b, Sound.metalImpactHeavy);
                    Break.Add(b, Sound.metalSnapLow);
                    ActionPiston.Add(b);
                    break;

                case BlockType.Hinge:
                    Break.Add(b, Sound.metalSnapLow);
                    break;

                case BlockType.WheelUnpowered:
                case BlockType.LargeWheelUnpowered:
                    Break.Add(b, Sound.metalSnapLow, 0.5f);
                    if (!isSmall && isCircular) {
                        Collision.Add(b, Sound.rollingSound);
                    }
                    break;

                case BlockType.CogMediumUnpowered:
                case BlockType.CogLargeUnpowered:
                    Break.Add(b, Sound.metalSnapLow, 0.5f);
                    break;

                case BlockType.SpinningBlock:
                    Collision.Add(b, Sound.metalImpactHeavy);
                    Break.Add(b, Sound.metalSnapLow, 0.5f);

                    ActionMotor.Add(b,
                        sound: Sound.servoSound,
                        volume: 0.1f,
                        pitch: Mathf.Clamp((b.BuildingBlock as CogMotorControllerHinge).SpeedSlider.Value, 0.0f, 5f));
                    break;

                case BlockType.SteeringBlock:
                case BlockType.SteeringHinge:
                    Collision.Add(b, Sound.metalImpactHeavy);
                    Break.Add(b, Sound.metalSnapLow, 0.5f);

                    ActionSteering.Add(b);
                    break;

                case BlockType.Wheel:
                case BlockType.LargeWheel:
                case BlockType.CogMediumPowered:
                    Break.Add(b, Sound.metalSnapLow, 0.5f);

                    float power = (b.BuildingBlock as CogMotorControllerHinge).SpeedSlider.Value;
                    if (!isSmall && isCircular) {
                        ActionMotor.Add(b,
                            sound: Sound.motorSound,
                            volume: 0.07f,
                            pitch: Mathf.Clamp(1f + ((power - 1) / 3.5f), 0f, 3f),
                            fadeDelta: 5 - Mathf.Clamp(power / 2, 0, 4));
                        Collision.Add(b, Sound.rollingSound);
                    }
                    break;

                case BlockType.Brace:
                    Break.Add(b, Sound.metalSnapMixed, 0.5f);
                    break;

                case BlockType.WaterCannon:
                    Collision.Add(b, Sound.metalImpactHeavy);
                    Break.Add(b, Sound.metalSnapLow);
                    ActionWaterCannon.Add(b);
                    break;

                case BlockType.Vacuum:
                    Collision.Add(b, Sound.metalImpactHeavy);
                    ActionVacuum.Add(b);
                    break;

                case BlockType.BuildSurface:
                    switch ((b.BuildingBlock as BuildSurface).material.Value) {

                        case 0:
                            if ((b.BuildingBlock as BuildSurface).Rigidbody.mass < 0.5f) {
                                Break.Add(b, Sound.woodSnapHigh);
                                Collision.Add(b, Sound.woodImpactLight);
                            }
                            else if ((b.BuildingBlock as BuildSurface).Rigidbody.mass < 2f) {
                                Break.Add(b, Sound.woodSnapMid);
                                Collision.Add(b, Sound.woodImpactHeavy);
                            }
                            else {
                                Break.Add(b, Sound.woodSnapLow);
                                Collision.Add(b, Sound.woodImpactHeavy);
                            }
                            break;

                        case 1:
                            Break.Add(b, Sound.glassSmash);
                            break;
                    }

                    break;

                case BlockType.Boulder:
                case BlockType.FlameBall:
                    Collision.Add(b, Sound.woodImpactHeavy);
                    break;

                case BlockType.Rocket:
                    Collision.Add(b, (isWheelShaped) ? Sound.rollingSound : Sound.woodImpactHeavy);
                    break;

                case BlockType.Torch:
                    Collision.Add(b, Sound.metalImpactLight);
                    Break.Add(b, Sound.metalSnapHigh);
                    break;

                case BlockType.StartingBlock:
                case BlockType.Sensor:
                    Collision.Add(b, Sound.metalImpactHeavy);
                    break;

                case BlockType.Plow:
                case BlockType.HalfPipe:
                case BlockType.MetalBall:
                    Collision.Add(b, Sound.metalImpactHeavy);
                    Break.Add(b, Sound.metalSnapLow);
                    break;

                case BlockType.Bomb:
                    b.gameObject.AddComponent<ActionBomb>();
                    break;

                case BlockType.RopeWinch:
                    ActionWinch.Add(b);
                    break;

                default:
                    if (b.BlockID > 999) {
                        Break.Add(b, Sound.metalSnapLow);
                        Collision.Add(b, Sound.metalImpactHeavy);
                    }
                    break;
            }
        }
    }
}
