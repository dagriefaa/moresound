﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MoreSound {
    class ActionBomb : MonoBehaviour {

        static bool thisFrame = false;

        void Update() {
            thisFrame = false;
        }

        void OnDisable() {
            if (!thisFrame) {
                thisFrame = true;
                ReferenceMaster.physicsGoalInstance.GetComponentsInChildren<ExplosionEffect>()
                    .Where(x => x.enabled && !x.GetComponent<ActionExplode>())
                    .ToList()
                    .ForEach(x => x.gameObject.AddComponent<ActionExplode>());
            }
        }
    }
}
