﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MoreSound {
    /// <summary>
    /// ActionContinuous implementation for VacuumBlock types.
    /// </summary>
    class ActionVacuum : ActionContinuous {

        public static ActionVacuum Add(BlockBehaviour b) {
            float power = Mathf.Abs((b.BuildingBlock as VacuumBlock).PowerSlider.Value);
            if (power > 1E20) {
                return ActionContinuous.Add<ActionVacuum>(b, 
                    sound: Sound.negativeWaterCannonSound, 
                    volume: 0.1f);
            }
            else {
                return ActionContinuous.Add<ActionVacuum>(b, 
                    sound: Sound.vacuumSound, 
                    volume: 0.3f, 
                    pitch: Mathf.Clamp(0.75f + (Mathf.Abs(power) * 0.25f), 0.0f, 2f));
            }
        }

        protected override bool IsPlaying() {
            return !(block as VacuumBlock).vacuumeController.isOff;
        }
    }
}
