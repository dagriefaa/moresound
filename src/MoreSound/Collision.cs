﻿using System.Collections.Generic;
using UnityEngine;

namespace MoreSound {
    class Collision : ActionSingle {

        static readonly float COLLISION_THRESHOLD_SQR = 6f;
        static readonly float COLLISION_WAIT_TIME = 0.5f;

        public static List<int> ActiveCollisionIndices = new List<int>();

        int clusterIndex;
        public float CollisionCooldown { get; private set; } = 0f;

        public static Collision Add(BlockBehaviour b, AudioClip collisionSound) {
            Collision res = ActionSingle.Add<Collision>(b, sound: collisionSound);
            res.clusterIndex = b.ClusterIndex;
            return res;
        }

        protected override void OnDisableSound() {
            ActiveCollisionIndices.Remove(clusterIndex);
        }

        protected override void UpdateAlways() {

            if (CollisionCooldown > 0f) {
                CollisionCooldown -= Time.unscaledDeltaTime;
                if (CollisionCooldown <= 0f) {
                    ActiveCollisionIndices.Remove(clusterIndex);
                }
            }

        }

        protected void OnCollisionEnter(UnityEngine.Collision other) {

            if (CollisionCooldown > 0.05f
                || ActiveCollisionIndices.Contains(block.ClusterIndex) || ActiveCollisionIndices.Contains(clusterIndex)
                || other.relativeVelocity.sqrMagnitude < COLLISION_THRESHOLD_SQR
                || soundWasPlaying) {
                return;
            }

            volumeBase = Mathf.Clamp(other.relativeVelocity.sqrMagnitude / 5000f, 0, 0.4f);
            PitchReal = Random.Range(0.5f, 1.5f);

            source.Play();
            CollisionCooldown = COLLISION_WAIT_TIME;
            ActiveCollisionIndices.Add(clusterIndex = block.ClusterIndex);

        }
    }
}
