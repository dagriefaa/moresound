﻿using UnityEngine;

namespace MoreSound {
    class ActionWinch : ActionContinuous {

        SpringCode winch;

        public static ActionWinch Add(BlockBehaviour b) {
            return ActionContinuous.Add<ActionWinch>(b,
                sound: Sound.servoSound,
                volume: 0.05f,
                pitch: Mathf.Clamp(Mathf.Abs((b.BuildingBlock as SpringCode).SpeedSlider.Value), 0.0f, 1.5f),
                fadeDelta: 8);
        }

        void Start() {
            winch = block as SpringCode;
        }

        protected override bool IsPlaying() {
            bool contractHeld = winch.ContractKey.IsHeld || winch.ContractKey.EmulationHeld(true);
            bool unwindHeld = winch.UnwindKey.IsHeld || winch.UnwindKey.EmulationHeld(true);

            return contractHeld ^ unwindHeld;
        }
    }
}
