﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MoreSound {
    class ActionExplode : ActionSingle {

        void Awake() {
            if (!source) {
                source = GetComponent<AudioSource>();
                volumeBase = 0.8f;
                PitchReal = pitchBase = Time.timeScale;

                source.spatialBlend = 1;
                source.rolloffMode = AudioRolloffMode.Custom;
                source.SetCustomCurve(AudioSourceCurveType.CustomRolloff, Sound.CONSTANT_CURVE);
            }
        }
    }
}
