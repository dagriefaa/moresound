﻿using System.Collections.Generic;
using UnityEngine;

namespace MoreSound {
    class Break : ActionSingle {

        float surfaceBreakForce;

        public static Break Add(BlockBehaviour b, AudioClip breakSound, float breakVariance = 0.2f) {
            float pitch = b.transform.localScale.x * b.transform.localScale.y * b.transform.localScale.z;
            Break res = ActionSingle.Add<Break>(b, 
                sound: breakSound, 
                pitch: Random.Range(1 - breakVariance, 1 + breakVariance) / ((breakSound == Sound.metalSnapHigh) ? 1 : Mathf.Clamp(pitch, 0.2f, 4f)),
                loop: false);

            return res;
        }

        void Start() {

            if (block is BuildSurface) {
                surfaceBreakForce = (block as BuildSurface).Joints?[0]?.breakForce ?? 10000f;
                (block as BuildSurface).FragmentController.OnBreak += () => OnJointBreak(surfaceBreakForce);
            }

        }

        public void OnJointBreak(float breakForce) {
            if (breakForce < 500 || !IsAudible()) {
                return;
            }
            volumeBase = Mathf.Clamp(breakForce * 0.00002f, 0.2f, 1f);
            source.PlayDelayed(Random.Range(0f, 0.2f));
        }


    }
}
