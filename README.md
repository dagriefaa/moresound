<div align="center">
<img src="header.png" alt="More Sound" width="500"/><br>
https://steamcommunity.com/sharedfiles/filedetails/?id=2073422152<br>
A mod for [Besiege](https://store.steampowered.com/app/346010), a physics based building sandbox game.
</div><br>

More Sound is a mod for [Besiege](https://store.steampowered.com/app/346010) which adds more sound (ha ha) to a mostly silent game. Dynamic block interaction, collision, and destruction noises have been added to most blocks. 

The mod is tuned to fit the game's existing sound design - subtle and minimalistic. Mostly.

## Features
- Breaking noises for blocks (that can break)
- Collision noises for blocks (rate limited, per cluster)
- Burning noises
- Noises for blocks that do things, with full support for vanilla-compatible (scaling, NoBounds) contexts:
    - Flying block (normal, high power, overflow)
    - Water cannon (negative, normal, steam, overflow)
    - Wheel (rolling and powered)
    - Flamethrower (normal, high power)
    - Circular saw
    - Drill
    - Steering hinge
    - Steering block
    - Spinning block
    - Vacuum (normal, overflow)
    - Cannon (high power)
    - Crossbow (high power)
    - Propeller (propeller, jet)
    - Winch
    - Decoupler
    - Piston
- Custom audio falloff (louder sounds can be heard farther away)
- Custom audio normalisation (volume scales with loudness - loud sounds won't blow up your speakers, but instead mask quiet noises)
- Automatic configuration based on block settings - no setup needed

## Technologies
* Unity 5.4 - the game engine Besiege runs on
* C# 3.0 - the scripting language that Unity 5.4 shipped with

## Installation
The latest version of the mod can be found on the Steam Workshop, and can be installed at the press of the big green 'Subscribe' button. It will automatically activate the next time you boot the game.

If, for whatever reason, you don't want to use Steam to manage your mods, installation is as follows:
1. Install Besiege.
2. Clone the repository into `Besiege/Besiege_Data/Mods`.
3. Open the `.sln` file in `src` in Visual Studio 2015 (or later).
4. Press F6 to compile the mod.

## Usage
The mod is plug-and-play - just install it. It will automatically adjust sounds based on the settings of the blocks.

Additional usage instructions can be found on the [Steam Workshop page](https://steamcommunity.com/sharedfiles/filedetails/?id=2073422152).

## Status
This iteration of the mod is complete and will not be developed further. <br>
