﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MoreSound {

    /// <summary>
    /// ActionContinuous implementation for FlamethrowerController types.
    /// </summary>
    class ActionFlamethrower : ActionContinuous {

        public static ActionFlamethrower Add(BlockBehaviour b) {
            float power = Mathf.Abs((b.BuildingBlock as FlamethrowerController).RangeSlider.Value);

            return ActionContinuous.Add<ActionFlamethrower>(b,
                sound: (power <= 10f) ? Sound.flamethrowerSoundLow : Sound.flamethrowerSoundHigh,
                volume: Mathf.Clamp(0.3f + power * 0.015f, 0.3f, 0.55f),
                pitch: Mathf.Clamp(power, 0.2f, 1.2f),
                fadeDelta: 6);
        }

        protected override bool IsPlaying() {
            FlamethrowerController flameSelf = block as FlamethrowerController;
            return flameSelf.isFlaming && (!flameSelf.timeOut || flameSelf.ParentMachine.InfiniteAmmoMode);
        }
    }
}
