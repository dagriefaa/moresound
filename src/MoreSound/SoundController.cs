﻿using UnityEngine;
using Modding;
using System.Collections;

public class SoundController : MonoBehaviour {

    static bool _timescalePitch = false;
    public static bool TimescaleAffectsPitch {
        get {
            return _timescalePitch;
        }
        set {
            if (_timescalePitch == value && !startup) return;
            _timescalePitch = value;
            Modding.Configuration.GetData().Write("TimescaleAffectsPitch", value);
        }
    }

    static bool startup = false;

    void Awake() {
        TimescaleAffectsPitch = Configuration.GetData().HasKey("TimescaleAffectsPitch") && Configuration.GetData().ReadBool("TimescaleAffectsPitch");

        startup = true;
    }
}
