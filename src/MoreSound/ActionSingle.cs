﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MoreSound {

    /// <summary>
    /// Base class for block state sounds which don't loop (e.g. cannon).
    /// </summary>
    abstract class ActionSingle : Sound {

        protected static T Add<T>(BlockBehaviour b, AudioClip sound, float volume = 0.4f, float pitch = 1f, AudioSource source = null) where T : ActionSingle {
            return Sound.Add<T>(b, 
                sound: sound, 
                volume: volume, 
                pitch: pitch, 
                source: source,
                loop: false);
        }

        protected virtual void UpdateAlways() {
        }

        protected sealed override void Update() {
            UpdateAlways();

            if (source.isPlaying) {
                Sound.ActiveVolumeTotal -= VolumeReal;
                VolumeReal = volumeBase * Attenuation(volumeBase);
                Sound.ActiveVolumeTotal += VolumeReal;
                source.volume = VolumeReal * Saturation();
                soundWasPlaying = true;

                source.pitch = PitchReal;
                if (SoundController.TimescaleAffectsPitch) {
                    source.pitch *= Time.timeScale;
                }
            }
            else if (!source.isPlaying && soundWasPlaying) {
                Sound.ActiveVolumeTotal -= VolumeReal;
                VolumeReal = 0f;
                soundWasPlaying = false;
            }

        }
    }
}
